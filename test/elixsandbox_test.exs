defmodule ElixsandboxTest do
  use ExUnit.Case
  doctest Elixsandbox

  test "the truth" do
    assert 1 + 1  == 2
  end
  test "fn uppercase" do
    assert(Elixsandbox.uppercase("senthil") == "SENTHIL")
  end

  test "fn lowercase" do
    assert(Elixsandbox.lowercase("Senthil") == "senthil")
  end

end