defmodule Elixsandbox do
    def uppercase(str) do
       String.upcase(str)
    end

    def lowercase(str) do
        String.downcase(str)
    end
end
