var gulp = require('gulp');
var exec = require('child_process').exec;
var chalk = require('chalk');
var chokidar = require('chokidar');

gulp.task('run-mix-test',function(){
    child = exec('mix test',
      function (error, stdout, stderr) {
        if (error !== null) {
          console.log(chalk.red(error));
        }
        console.log(chalk.blue(stdout));
    });
});

gulp.task('watch',function(){
    watcher(['lib/**/*','test/**/*'], function(){
        gulp.start('run-mix-test');
    });
});

gulp.task('default',['watch','run-mix-test']);

function watcher(fileOrDirectory, callBack) {
	return chokidar.watch(fileOrDirectory, {ignored: /[\/\\]\./, ignoreInitial: true})
		.on('add', function() {
			console.log("Event:ADD");
			callBack();
		})
		.on('change', function() {
			console.log("Event:CHANGE");
			callBack();
		})
		.on('unlink', function() {
			console.log("Event:DELETE");
			callBack();
		})
		.on('error', function(error) {
			console.error('Error happened', error);
		})
}